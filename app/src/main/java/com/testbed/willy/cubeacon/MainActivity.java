package com.testbed.willy.cubeacon;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.testbed.willy.R;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Willy on 6/6/2015.
 */
public class MainActivity extends Activity implements View.OnClickListener{
    private static final String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog myLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_cubeacon);
        Button start_button = (Button) findViewById(R.id.button_start_2);
        Button setting_button = (Button) findViewById(R.id.button_setting_2);
        Button exit_button = (Button) findViewById(R.id.button_exit_2);
        start_button.setOnClickListener(this);
        setting_button.setOnClickListener(this);
        exit_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_start_2:
                Log.v(TAG, "Start Computing");
                new MyProgressDialog().execute();
                break;
            case R.id.button_exit_2:
                Log.v(TAG,"See You Again");
                MainActivity.this.finish();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Create Default Setting
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Snowbyte/Cubeacon");
            if (!root.exists()) {
                root.mkdirs();
            }
            File myfile = new File(root, "settings");
            if(!myfile.exists()) {
                FileWriter writer = new FileWriter(myfile);
                writer.append("3\n");
                writer.append("false");
                writer.flush();
                writer.close();
            }
        }
        catch (IOException ie){
            Log.e(TAG, "" + ie);
        }
    }
    private class MyProgressDialog extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myLoading = new ProgressDialog(MainActivity.this);
            myLoading.setTitle("Starting");
            myLoading.setMessage("Waiting....");
            myLoading.setCancelable(false);
            myLoading.show();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try{
                Thread.sleep(10000);
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            myLoading.dismiss();
            Log.v(TAG, "Start Computing");
            Intent start = new Intent(MainActivity.this, com.testbed.willy.cubeacon.CubeSplash.class);
            startActivity(start);
        }
    }
}
