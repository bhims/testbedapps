package com.testbed.willy.cubeacon;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.testbed.willy.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by R0311MDN3 on 18/02/2015.
 */
public class Settings extends Activity {
    private final static String TAG = Settings.class.getSimpleName();
    private Spinner spinner1;
    private View cancel_button;
    private View apply_button;
    private CheckBox myCheckbox;
    private String current_setting;
    private String GPS_setting;
    private String[] list_beacon = {"Tekno 1","Tekno 2","Tekno 3","GRxb","Phhp","qR4Q"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.v(TAG,"Position :"+position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        myCheckbox = (CheckBox) findViewById(R.id.checkbox1);

        cancel_button = findViewById(R.id.cancel_button);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.this.finish();
            }
        });
        apply_button = findViewById(R.id.apply_button);
        apply_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    File root = new File(Environment.getExternalStorageDirectory(), "Snowbyte/Cubeacon");
                    if (!root.exists()) {
                        //Confirmation
                    }
                    File myfile = new File(root, "settings");
                    if (myfile.exists()) {
                        FileWriter writer = new FileWriter(myfile);
                        writer.append(""+spinner1.getSelectedItemPosition()+"\n");
                        writer.append(""+myCheckbox.isChecked());
                        Log.v(TAG,""+spinner1.getSelectedItemPosition());
                        writer.flush();
                        writer.close();
                    }
                }
                catch (IOException ie){
                    Log.e(TAG,""+ie);
                }
                Settings.this.finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Snowbyte/Cubeacon");
            if (!root.exists()) {
                //Confirmation
            }
            File myfile = new File(root, "settings");
            if (myfile.exists()) {
                FileReader reader = new FileReader(myfile);
                BufferedReader bfr = new BufferedReader(reader);
                current_setting = bfr.readLine();
                GPS_setting = bfr.readLine();
                Log.v(TAG,""+GPS_setting);
                if(GPS_setting.equals("true")){
                    myCheckbox.setChecked(true);
                    Log.v(TAG,"Great Coming");
                }
                else{
                    myCheckbox.setChecked(false);
                }
                spinner1.setSelection(Integer.parseInt(current_setting));
                reader.close();
            }
        }
        catch(IOException ie){
            Log.e(TAG,""+ie);
        }
    }
}
