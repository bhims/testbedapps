package com.testbed.willy.cubeacon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.eyro.cubeacon.CBApp;
import com.eyro.cubeacon.CBConstant;
import com.testbed.willy.R;

/**
 * Created by Willy on 7/12/2015.
 */
public class CubeSplash extends Activity {
    private TextView	txtLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cube_splash);
        txtLoading = (TextView) findViewById(R.id.txtProgressBarSplash);

        txtLoading.setText("Initializing Cubeacon");
        CBApp.refreshBeaconInBackground(this, new CBConstant.RefreshCallback() {

            @Override
            public void onRefreshCompleted(Exception e) {
                if (e == null) {
                    Intent intent = new Intent(CubeSplash.this,
                            StartTest.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Log.e("SplashActivity", e.getMessage());
                }
            }
        });
    }
}
