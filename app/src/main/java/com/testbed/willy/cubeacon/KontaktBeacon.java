package com.testbed.willy.cubeacon;

/**
 * Created by Willy on 2/18/2015.
 */
public class KontaktBeacon {
    private int MajorValueGRxb = 30187;
    private int MinorValueGRxb = 2166;
    private int MajorValuePhhp = 23167;
    private int MinorValuePhhp = 24154;
    private int MajorValueqR4Q = 14896;
    private int MinorValueqR4Q = 43120;

    public int getMajorKontakt(int id){
        int majorValue = 0;
        switch (id){
            case 3:
                majorValue = MajorValueGRxb;
                break;
            case 4:
                majorValue = MajorValuePhhp;
                break;
            case 5:
                majorValue = MajorValueqR4Q;
                break;
        }
        return majorValue;
    }
    public int getMinorKontakt(int id){
        int minorValue = 0;
        switch (id){
            case 3:
                minorValue = MinorValueGRxb;
                break;
            case 4:
                minorValue = MinorValuePhhp;
                break;
            case 5:
                minorValue = MinorValueqR4Q;
                break;
        }
        return minorValue;
    }
    public String getKontaktName(int id){
        String name = null;
        switch (id) {
            case 3:
                name = "GRxb";
                break;
            case 4:
                name = "Phhp";
                break;
            case 5:
                name = "qR4Q";
                break;
        }
        return name;
    }
}
