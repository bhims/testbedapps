package com.testbed.willy.kontakt;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.kontakt.sdk.android.configuration.BeaconActivityCheckConfiguration;
import com.kontakt.sdk.android.configuration.MonitorPeriod;
import com.kontakt.sdk.android.connection.OnServiceBoundListener;
import com.kontakt.sdk.android.data.RssiCalculators;
import com.kontakt.sdk.android.device.BeaconDevice;
import com.kontakt.sdk.android.device.Region;
import com.kontakt.sdk.android.factory.Filters;
import com.kontakt.sdk.android.manager.ActionManager;
import com.kontakt.sdk.android.manager.BeaconManager;
import com.testbed.willy.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created by Willy on 2/12/2015.
 */
public class StartTest extends ActionBarActivity implements View.OnClickListener {
    public static final String TAG = StartTest.class.getSimpleName();
    private static final int REQUEST_CODE_ENABLE_BLUETOOTH = 1;
    private static final int REQUEST_CODE_CONNECT_TO_DEVICE = 2;
//    private Region region = new Region(null, 0,null);
    private BeaconManager beaconManager;
    private ActionManager actionManager;
    private int BeaconId = 3;
    private String current_beacon;
    KontaktBeacon kontaktBeacon = new KontaktBeacon();
    EstimoteBeacon estimoteBeacon = new EstimoteBeacon();
    Cubeacon cubeacon = new Cubeacon();
    String currentBeaconName = null;
    String beaconDistance;
    String GPS_setting;
    Long currenttime;
    String[] myData = new String[1000000];
    int counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);
        final View stopRecordingButton = findViewById(R.id.button_stop_record);
        final GPSTracker gpsTracker = new GPSTracker(StartTest.this);
        stopRecordingButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //Beacon Manager Default Setting
        beaconManager = BeaconManager.newInstance(this);
        currenttime = System.currentTimeMillis();
        beaconManager.setBeaconActivityCheckConfiguration(BeaconActivityCheckConfiguration.DEFAULT);
        beaconManager.setRssiCalculator(RssiCalculators.newLimitedMeanRssiCalculator(1)); //Calculate rssi value basing on arithmethic mean of 5 last notified values
        beaconManager.setMonitorPeriod(MonitorPeriod.MINIMAL);
        beaconManager.registerMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onMonitorStart() {

            }

            @Override
            public void onMonitorStop() {

            }

            @Override
            public void onBeaconsUpdated(Region region, final List<BeaconDevice> beaconDevices) {
                Log.v(TAG, ""+beaconDevices.get(0).getBeaconUniqueId());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    if(!beaconDevices.isEmpty()) {
                        Long newTime = System.currentTimeMillis();
                        Long timeellapse = newTime - currenttime;
                        Log.v(TAG,"Time Ellapse"+ timeellapse);
                        if(timeellapse >= 2400000){
                            StartTest.this.onClick(stopRecordingButton);
                        }
                        beaconDistance = String.format("%.2f", beaconDevices.get(0).getAccuracy());
                        TextView tv1 = (TextView)findViewById(R.id.main_text);
                        TextView tv2 = (TextView)findViewById(R.id.second_text);
                        tv1.setText(""+beaconDevices.get(0).getRssi()+" dbm");
                        tv2.setText(""+beaconDistance+" m");
                        counter++;
                        if(GPS_setting.equals("true")){
                            myData[counter]=""+beaconDevices.get(0).getRssi()+"#"+beaconDistance+"#"+gpsTracker.getLatitude()+"#"+gpsTracker.getLongitude();
                        }
                        else{
                            //Add time stamp
                            myData[counter]=""+beaconDevices.get(0).getRssi()+"#"+beaconDistance+"#"+newTime;
                        }
                    }

                    }
                });
            }

            @Override
            public void onBeaconAppeared(Region region, final BeaconDevice beaconDevice) {
            }

            @Override
            public void onRegionEntered(Region region) {

            }

            @Override
            public void onRegionAbandoned(Region region) {

            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE_ENABLE_BLUETOOTH) {
            if(resultCode == Activity.RESULT_OK) {
                connect();
            } else {
//                final String bluetoothNotEnabledInfo = getString(R.string.bluetooth_not_enabled);
//                Toast.makeText(this, bluetoothNotEnabledInfo, Toast.LENGTH_LONG).show();
//                getSupportActionBar().setSubtitle(bluetoothNotEnabledInfo);
            }
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
    private void connect() {
        try {
            beaconManager.connect(new OnServiceBoundListener() {
                @Override
                public void onServiceBound() {
                    try {
                        beaconManager.startMonitoring();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (RemoteException e) {
            Log.e(TAG,""+e);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_stop_record:
                Log.v(TAG,"Stop Recording");
                try
                {
                    Time now = new Time();
                    now.setToNow();
                    File root = new File(Environment.getExternalStorageDirectory(), "Snowbyte/Kontakt");
                    if (!root.exists()) {
                        root.mkdirs();
                    }
                    File myfile = new File(root, ""+currentBeaconName+"_"+now.hour+"_"+now.minute+".txt");
                    FileWriter writer = new FileWriter(myfile);
                    for(int i=1;i<=counter;i++) {
                        writer.append(myData[i]);
                        writer.append("|");
                        writer.flush();
                    }
                    writer.close();
                    Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
                StartTest.this.finish();
        }
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Snowbyte/Kontakt");
            if (!root.exists()) {
                //Confirmation
            }
            File myfile = new File(root, "settings");
            if (myfile.exists()) {
                FileReader reader = new FileReader(myfile);
                BufferedReader bfr = new BufferedReader(reader);
                current_beacon = bfr.readLine();
                GPS_setting = bfr.readLine();
                BeaconId = Integer.parseInt(current_beacon);
                reader.close();
            }
        }
        catch(IOException ie){
            Log.e(TAG,""+ie);
        }
        if (BeaconId <3) {
            beaconManager.addFilter(Filters.newMultiFilterBuilder()
                    .setProximityUUID(UUID.fromString(cubeacon.getUUID()))
                    .setMajor(cubeacon.getMajorCube(BeaconId))
                    .setMinor(cubeacon.getMinorCube(BeaconId)).build());
            currentBeaconName = cubeacon.getCubeName(BeaconId);
        }
        else{
            if(BeaconId >=3 && BeaconId <6){
                beaconManager.addFilter(Filters.newMultiFilterBuilder()
                        .setProximityUUID(UUID.fromString(estimoteBeacon.getUUIDEstimote()))
                        .setMajor(estimoteBeacon.getMajorEstimote(BeaconId))
                        .setMinor(estimoteBeacon.getMinorEstimote(BeaconId)).build());
                currentBeaconName = estimoteBeacon.getEstimoteName(BeaconId);
            }
            else{
                beaconManager.addFilter(Filters.newProximityUUIDFilter(BeaconManager.DEFAULT_KONTAKT_BEACON_PROXIMITY_UUID));
                beaconManager.addFilter(Filters.newMajorFilter(kontaktBeacon.getMajorKontakt(BeaconId)));
                beaconManager.addFilter(Filters.newMinorFilter(kontaktBeacon.getMinorKontakt(BeaconId)));
                currentBeaconName = kontaktBeacon.getKontaktName(BeaconId);
            }
        }
        TextView tv3 = (TextView)findViewById(R.id.destination);
        tv3.setText(currentBeaconName);
        if (!beaconManager.isBluetoothEnabled()) {
            final Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_CODE_ENABLE_BLUETOOTH);
        } else if (beaconManager.isConnected()) {
            try {
                Log.v(TAG,"Berhasil");
                beaconManager.startMonitoring(); // starts monitoring everywhere

                /*final Set<Region> regionSet = new HashSet<Region>();
                                regionSet.add(new Region(UUID.randomUUID(), 333, 333, "My region"));
                                beaconManager.startMonitoring(regionSet);

                                You can monitor Beacons by specifying Region Set as it was
                                in previous versions of kontakt.io's Android SDK
                                */
            } catch (RemoteException e) {
                Log.e(TAG,""+e);
            }
        } else {
            connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        beaconManager.stopMonitoring();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        beaconManager.disconnect();
        beaconManager = null;
    }
}
