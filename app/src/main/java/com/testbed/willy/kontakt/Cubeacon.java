package com.testbed.willy.kontakt;

/**
 * Created by Willy on 6/7/2015.
 */
public class Cubeacon {
    private final String UUID ="CB10023F-A318-3394-4199-A8730C7C1AEC";
    private int RedMajor = 1;
    private int RedMinor = 284;
    private int GreenMajor = 2;
    private int GreenMinor = 284;
    private int BlueMajor = 3;
    private int BlueMinor = 284;
    public String getUUID(){
        return UUID;
    }
    public int getMajorCube(int id){
        switch (id){
            case 0:
                return RedMajor;
            case 1:
                return GreenMajor;
            case 2:
                return BlueMajor;
            default:
                return 0;
        }
    }
    public int getMinorCube(int id){
        switch (id){
            case 0:
                return RedMinor;
            case 1:
                return GreenMinor;
            case 2:
                return BlueMinor;
            default:
                return 0;
        }
    }
    public String getCubeName(int id){
        String name = null;
        switch (id){
            case 0:
                name = "RED";
                break;
            case 1:
                name = "GREEN";
                break;
            case 2:
                name = "BLUE";
                break;
        }
        return name;
    }
}
