package com.testbed.willy.kontakt;

/**
 * Created by Willy on 2/18/2015.
 */
public class EstimoteBeacon {
    private final String UUID_Value = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    private int MajorValueTekno1 = 61357;
    private int MinorValueTekno1 = 41599;
    private int MajorValueTekno2 = 40427;
    private int MinorValueTekno2 = 57206;
    private int MajorValueTekno3 = 15984;
    private int MinorValueTekno3 = 12715;

    public String getUUIDEstimote(){
        return UUID_Value;
    }
    public int getMajorEstimote(int id){
        int majorValue = 0;
        switch (id){
            case 3:
                majorValue = MajorValueTekno1;
                break;
            case 4:
                majorValue = MajorValueTekno2;
                break;
            case 5:
                majorValue = MajorValueTekno3;
                break;
        }
        return majorValue;
    }
    public int getMinorEstimote(int id){
        int minorValue = 0;
        switch (id){
            case 3:
                minorValue = MinorValueTekno1;
                break;
            case 4:
                minorValue = MinorValueTekno2;
                break;
            case 5:
                minorValue = MinorValueTekno3;
                break;
        }
        return minorValue;
    }
    public String getEstimoteName(int id){
        String name = null;
        switch (id){
            case 3:
                name = "TEKNO1";
                break;
            case 4:
                name = "TEKNO2";
                break;
            case 5:
                name = "TEKNO3";
                break;
        }
        return name;
    }

}
