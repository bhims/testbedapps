package com.testbed.willy.kontakt;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.testbed.willy.R;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class MainActivity extends Activity implements View.OnClickListener {
    private final static String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog myLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View buttonStart = findViewById(R.id.button_start);
        buttonStart.setOnClickListener(this);
        View buttonSetting = findViewById(R.id.button_setting);
        buttonSetting.setOnClickListener(this);
        View buttonExit = findViewById(R.id.button_exit);
        buttonExit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_start:
                Log.v(TAG, "Start Computing");
                new MyProgressDialog().execute();
                break;
            case R.id.button_setting:
                Log.v(TAG,"This is Setting Menu");
                Intent setting = new Intent(MainActivity.this,Settings.class);
                startActivity(setting);
                break;
            case R.id.button_exit:
                Log.v(TAG,"See You Again");
                MainActivity.this.finish();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    //Create Default Setting
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Snowbyte/Kontakt");
            if (!root.exists()) {
                root.mkdirs();
            }
            File myfile = new File(root, "settings");
            if(!myfile.exists()) {
                FileWriter writer = new FileWriter(myfile);
                writer.append("3\n");
                writer.append("false");
                writer.flush();
                writer.close();
            }
        }
        catch (IOException ie){
            Log.e(TAG,""+ie);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private class MyProgressDialog extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myLoading = new ProgressDialog(MainActivity.this);
            myLoading.setTitle("Starting");
            myLoading.setMessage("Waiting....");
            myLoading.setCancelable(false);
            myLoading.show();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try{
                Thread.sleep(10000);
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            myLoading.dismiss();
            Log.v(TAG, "Start Computing");
            Intent start = new Intent(MainActivity.this,StartTest.class);
            startActivity(start);
        }
    }
}
