package com.testbed.willy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by Willy on 6/5/2015.
 */
public class BeginOption extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.option_start);
        ImageButton kontakt_option = (ImageButton) findViewById(R.id.kontaktsym);
        ImageButton cubeacon_option = (ImageButton) findViewById(R.id.cubeaconsym);

        //Implement onClick Listener
        kontakt_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kontakt_option = new Intent(BeginOption.this, com.testbed.willy.kontakt.MainActivity.class);
                startActivity(kontakt_option);

            }
        });
        cubeacon_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cube_option = new Intent(BeginOption.this, com.testbed.willy.cubeacon.MainActivity.class);
                startActivity(cube_option);
            }
        });
    }
    private void ShowDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Confirmation");
        dialog.setMessage("Cubeacon SDK has so many flaws, We are still inspecting it");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
